import unittest
from fractions import Fraction
from functools import reduce


def to_float(value: str):
    if value in ('', ' '):
        return 0
    return float(value)


def input_data():
    line1 = input()
    n, m = (int(d) for d in line1.split())

    A = []
    B = []
    for i in range(0, n):
        line = input().split()
        B.append(to_float(line.pop()))
        A.append([to_float(d) for d in line])
    return n, m, A, B


def output(solution_type, X):
    print(solution_type)
    print(' '.join((str(x) for x in X)))


def _rearrange(A, B, i_zero):
    # TODO:  handle case when there are no A[i_zero+1]
    A[i_zero], A[i_zero + 1] = A[i_zero + 1], A[i_zero]
    B[i_zero], B[i_zero + 1] = B[i_zero + 1], B[i_zero]

    return A, B


class InfiniteSolutionsCount(Exception):
    pass


class ZeroSolutionsCount(Exception):
    pass


def _has_solutions(A, B, line_num, is_extra_equation):
    if all((a == 0 for a in A[line_num])):
        if B[line_num] == 0:
            if not is_extra_equation:
                raise InfiniteSolutionsCount()
        else:
            raise ZeroSolutionsCount()


def is_zero(num):
    return num <= 0.000000000000001


def _has_one_solution(A, n, m):
    for i in range(0, m):
        main_variables_count = 0
        for j in range(0, m):
            if j < i:
                if A[i][j] != 0:
                    raise InfiniteSolutionsCount()
            else:
                main_variables_count = main_variables_count + 1
        if main_variables_count > n - i:
            raise InfiniteSolutionsCount()


def solve_by_gaus(n, m, A, B):
    matrix_min_size = min(m-1, n-1)
    for i in range(0, matrix_min_size + 1):
        if A[i][i] == 0:
            A, B = _rearrange(A, B, i)

        for j in range(i+1, n):
            if A[j][i] == 0:
                continue
            mult = Fraction(Fraction(A[j][i]), Fraction(A[i][i]))
            for k in range(i, m):
                A[j][k] = A[j][k] - mult*A[i][k]
                if k == i:
                    assert is_zero(A[j][k])

            B[j] = B[j] - mult*B[i]

            try:
                _has_solutions(A, B, j, j >= m)
            except InfiniteSolutionsCount:
                return 'INF', []
            except ZeroSolutionsCount:
                return 'NO', []

    try:
        _has_one_solution(A, n, m)
    except InfiniteSolutionsCount:
        return 'INF', []

    X = [0 for x in range(0, m)]
    for i in range(matrix_min_size, -1, -1):
        sub = reduce(lambda acc, z: acc + z[0]*z[1], zip(X[m-1:i:-1], A[i][m-1:i:-1]), 0)
        X[i] = (B[i] - sub) / A[i][i]

    return 'YES', [float(x) for x in X]


if __name__ == '__main__':
    equations_count, variables_count, coefficients, right_part = input_data()
    solution_type, X = \
        solve_by_gaus(equations_count, variables_count, coefficients, right_part)
    output(solution_type, X)


class GausTest(unittest.TestCase):
    ACCURACY = 7

    def _test_run(self, n, m, A, B, expectedST, expectedX):
        solution_type, X = solve_by_gaus(n, m, A, B)

        self.assertEqual(solution_type, expectedST)
        self.assertEqual(
            [round(x, self.ACCURACY) for x in expectedX],
            [round(x, self.ACCURACY) for x in X]
        )

    def test_normal(self):
        A = [
               [1, 1, 1],
               [1, 2, 2],
               [2, 3, -4]
           ]
        B = [6, 11, 3]

        self._test_run(3, 3, A, B, 'YES', [1, 3, 2])

    def test_normal2(self):
        A = [
            [4, 2, 1],
            [7, 8, 9],
            [9, 1, 3]
        ]
        B = [1, 1, 2]

        X = [0.2608695652173913, 0.04347826086956526, -0.1304347826086957]
        self._test_run(3, 3, A, B, 'YES', X)

    def test_normal3(self):
        A = [
            [6, 1, 2],
            [4, -6, 16],
            [3, 8, 1]
        ]
        B = [21, 2, 2]

        X = [4.133333333, -1.133333333, -1.333333333]
        self._test_run(3, 3, A, B, 'YES', X)

    def test_normal4(self):
        A = [
            [5, -3, 2, -8],
            [1, 1, 1, 1],
            [3, 5, 1, 4],
            [4, 2, 3, 1],
        ]
        B = [1, 0, 0, 3]

        X = [7, -8, -5, 6]
        self._test_run(4, 4, A, B, 'YES', X)

    def test_normal5(self):
        A = [
            [1, 2, 3],
            [3, 5, 7],
            [1, 3, 4]
        ]
        B = [3, 0, 1]

        X = [-4, -13, 11]
        self._test_run(3, 3, A, B, 'YES', X)

    def test_normal6(self):
        A = [
            [2, 5, 4, 1],
            [1, 3, 2, 1],
            [2, 10, 9, 7],
            [3, 8, 9, 2]
        ]
        B = [20, 11, 40, 37]

        X = [1, 2, 2, 0]
        self._test_run(4, 4, A, B, 'YES', X)

    def test_normal7(self):
        A = [
            [1, -2, 1],
            [2, 2, -1],
            [4, -1, 1]
        ]
        B = [0, 3, 5]

        X = [1, 2, 3]
        self._test_run(3, 3, A, B, 'YES', X)

    def test_normal8(self):
        A = [
            [3, 2, 1, 1],
            [1, -1, 4, -1],
            [-2, -2, -3, 1],
            [1, 5, -1, 2]
        ]
        B = [-2, -1, 9, 4]

        X = [-3, -1, 2, 7]
        self._test_run(4, 4, A, B, 'YES', X)

    def test_normal9(self):
        A = [
            [0.12, 0.18, -0.17],
            [0.06, 0.09, 0.15],
            [0.22, -0.1, 0.06]
        ]
        B = [5.5, -1.95, 0.5]

        X = [10, 5, -20]
        self._test_run(3, 3, A, B, 'YES', X)

    def test_normal10(self):
        A = [
            [0, 2, -1],
            [1, -1, 5],
            [2, 1, -1],
            [3, 2, 3],
            [3, 4, 2]
        ]
        B = [-4, 3, 0, -1, -5]

        X = [1, -2, 0]
        self._test_run(5, 3, A, B, 'YES', X)

    def test_range_min(self):
        A = [[-3]]
        B = [15]

        solution_type, X = solve_by_gaus(1, 1, A, B)

        self.assertEqual(solution_type, 'YES')
        self.assertEqual([-5], X)

    def test_zero_coeffs(self):
        A = [
               [0, -2, 2],
               [4, 7, 5],
               [2, 3, 1]
           ]
        B = [0, 20, 8]

        solution_type, X = solve_by_gaus(3, 3, A, B)

        self.assertEqual(solution_type, 'YES')
        self.assertEqual([2, 1, 1], X)

    def test_no_solutions(self):
        A = [
            [2, -1, 3],
            [2, -1, -1],
            [4, -2, 6],
            [6, 8, -7]
        ]
        B = [1, -2, 0, 2]

        solution_type, X = solve_by_gaus(4, 3, A, B)

        self.assertEqual(solution_type, 'NO')
        self.assertEqual(X, [])

    def test_no_solutions2(self):
        A = [
               [1, 3, 2],
               [2, 6, 4],
               [1, 4, 3]
           ]
        B = [7, 8, 1]

        self._test_run(3, 3, A, B, 'NO', [])

    def test_no_solutions3(self):
        A = [
               [3, -5, 2, 4],
               [7, -4, 1, 3],
               [5, 7, -4, -6]
           ]
        B = [2, 5, 3]

        self._test_run(3, 4, A, B, 'NO', [])

    def test_no_solutions4(self):
        A = [
            [7, -3, -4],
            [8, 7, 9],
            [3, 6, -10],
            [5, -5, 4],
            [-7, 2, 3]
        ]
        B = [-10, 2, -3, -2, 0]

        self._test_run(5, 3, A, B, 'NO', [])

    def test_no_solutions5(self):
        A = [
            [3, -5, 2, 4],
            [7, -4, 1, 3],
            [5, 7, -4, -6]
        ]
        B = [2, 5, 3]

        self._test_run(3, 4, A, B, 'NO', [])

    def test_inf(self):
        A = [
            [1, 3, 4],
            [2, 1, 4]
        ]
        B = [4, 5]

        solution_type, X = solve_by_gaus(2, 3, A, B)

        self.assertEqual(solution_type, 'INF')
        self.assertEqual(X, [])

    def test_inf2(self):
        A = [
            [1, 2, 1, 1, 3, 1],
            [1, 2, 1, 2, 1, -1],
            [1, 2, 1, -1, 5, -1],
            [1, 2, 1, -2, -4, -4]
        ]
        B = [7, 1, 2, -1]

        solution_type, X = solve_by_gaus(4, 6, A, B)

        self.assertEqual(solution_type, 'INF')
        self.assertEqual(X, [])

    def test_inf3(self):
        A = [
            [3, -2, 1],
            [5, -14, 15],
            [1, 2, -3]
        ]
        B = [0, 0, 0]

        solution_type, X = solve_by_gaus(3, 3, A, B)

        self.assertEqual(solution_type, 'INF')
        self.assertEqual(X, [])

    def test_inf4(self):
        A = [
            [2, 3, -1, 1],
            [2, 7, -3, 0],
            [0, 4, -2, -1],
            [2, -1, 1, 2],
            [4, 10, -4, 1]
        ]
        B = [0, 1, 1, -1, 1]

        solution_type, X = solve_by_gaus(5, 4, A, B)

        self.assertEqual(solution_type, 'INF')
        self.assertEqual(X, [])

    # other special cases:
    # http://www.cleverstudents.ru/systems/solving_systems_Gauss_method.html
    # http://www.mathprofi.ru/slu_nesovmestnye_sistemy_i_sistemy_s_obshim_resheniem.html
    # http://www.mathprofi.ru/metod_gaussa_dlya_chainikov.html
    # to check: https://matrixcalc.org/slu.html